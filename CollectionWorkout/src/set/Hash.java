package set;
import java.util.*;
public class Hash {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HashSet<String>s=new HashSet<String>();
		s.add("orange");
		s.add("apple");
		s.add("Berries");
		s.add("guava");
		s.add("green apple");
		s.add("Berries");           // Doesn't allow duplicate
		s.add(null);				//multiple no of nulls it consider single only
		
		System.out.println(s);
		
		 /*// Creating an Array
	     String[] array = new String[s.size()];
	     s.toArray(array);
	     
	     //Convert Hashset to TreeSet
	     TreeSet<String> tset = new TreeSet<String>(s);
	     System.out.println(tset);
	     */
	}

}
