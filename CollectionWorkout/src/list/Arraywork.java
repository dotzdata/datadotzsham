package list;
import java.util.*;



public class Arraywork {
public static void main(String[] args) {
	
	
	ArrayList<String>j=new ArrayList<String>();
	// Add the element to the list
	
	j.add("Java");
	j.add("Php");
	j.add("Bigdata");
	j.add("Dotnet");
	j.add("python");
	j.add("Javap");
	
	System.out.println(j);
	
/*	j.contains("java");        //check whether element present or not it return boolean result
	j.get(1);			       //Its return the value based on the index
	j.indexOf("Bigdata");	   //check whether element present or not it return index position
	j.remove("php");		   //Removes the element from list
	j.remove(3);			   //Based on the index position its remove the element
	j.hashCode();			   //the memory address to a certain extent.
	j.lastIndexOf("Bigdata");  // it return last element index position
	j.set(2, "Tableau");	   //Update the oldest one
	j.size();			       //Its return the size of the element
	j.trimToSize();            //this operation to minimize the storage of an ArrayList instance. 
	j.subList(2,4);     	   //used to return a view of the portion of this List between the range 
	j.isEmpty();			    //Returns true if this list contains no elements.
	j.addAll(c);			    //Returns true if this list contains no elements.
	j.containsAll(c);			//Returns true if this collection contains all of the elements in the specified collection. 
	j.clear();				    //Its remove all the element in the list
	j.clone();					//Shallow copy of the arrayList
	j.retainAll(c);	     		//removes from this list all of its elements that are not contained in the specified collection
	j.iterator();				//Returns an iterator over the elements in this list in proper sequence. 
	*/
	
	
	
}
}
