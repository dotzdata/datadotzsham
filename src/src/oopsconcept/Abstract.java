package src.oopsconcept;

public class Abstract {
	public static void main(String[] args) {
		Myclass m=new Myclass();
		m.get_data();
		m.disp_data();
		m.display();
	}
}
abstract class Abstraction{
	abstract void get_data();
	abstract void disp_data();
	void display(){
		System.out.println("Hi This is Abstraction Concept");
	}
}
class Myclass extends Abstraction{
	void get_data(){
		System.out.println("abstract method 1");
	}
	void disp_data(){
		System.out.println("abstract method 2");
	}
}
