package src.exception;

public class Try_Catch {
	public static void main(String[] args) {
		Mytry m=new Mytry();
		m.show();
	}

}
class Mytry{
	int a[]={11,12,13};
	void show() throws ArrayIndexOutOfBoundsException,NullPointerException  {
		try{
			System.out.println("The value is:"+a[4]);
			throw new ArrayIndexOutOfBoundsException("Trying to access out of memory data");
		}
		catch(ArrayIndexOutOfBoundsException  e){
			System.out.println(e);
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			System.out.println("Program ended");
		}
	}
	
}