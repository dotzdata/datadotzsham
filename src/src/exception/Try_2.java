package src.exception;


class Try_2
{
 static void check() throws ArithmeticException
 {
  System.out.println("Inside check function");
  throw new ArithmeticException("demo");
 }

 public static void main(String args[])
 {
  try
  {
   check();
  }
  catch(ArithmeticException e)
  {
   System.out.println("caught   :" + e);
  }
  finally{
	  System.out.println("End of program");
  }
 }
}
