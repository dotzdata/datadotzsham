package src.exception;

public class Try_1
{
 static void avg() 
 {
  try
  {
	  //throw new NullPointerException("demo");
  throw new ArithmeticException("demo");//Throws the Arithmetic Exceptions and displays demo if not handled
  }
  catch(Exception e){
	  System.out.println("Error");
  }
/*  catch(ArrayIndexOutOfBoundsException e)
  {
   System.out.println("Exception caught");
  } */
 /*catch(ArithmeticException e)//If handled the thrown exception is handled by this block
  {
	  System.out.println("Exception caught"); 
  }*/
 }

 public static void main(String args[])
 {
  avg(); 
 }
}
