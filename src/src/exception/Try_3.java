package src.exception;

class Try_3
{
 public static void main(String[] args)
 {
  int a[]= new int[2];
  System.out.println("out of try");
  try 
  {
   System.out.println("Access invalid element"+ a[3]);
   throw new ArrayIndexOutOfBoundsException("the above statement will throw ArrayIndexOutOfBoundException");
  }
  catch(ArrayIndexOutOfBoundsException e)
  {
	  System.out.println("Exception Caught"+e);
  }
  finally 
  {
   System.out.println("finally is always executed.");
  }
 }
}
