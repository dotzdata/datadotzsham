package ddd;
// Abstract code
abstract class Apple{
	void sample(){
		System.out.println("Normal method");
	}
	abstract void sample1();
	abstract void temp();
}


 class Abs extends Apple{
	void temp(){
		System.out.println("temp class ");
	}
	void sample1(){
		System.out.println("hello");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Abs u = new Abs();
		u.temp();
		u.sample();
		u.sample1();
	}

}
