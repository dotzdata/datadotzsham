package ddd;

class start{
		// TODO Auto-generated method stub
		public int a = 10;
		protected int b = 20;
		private int c = 30;
		
		void privateDisplay() {

			c = c+100;
			System.out.println("Private Variable : "+c);
		}
	}
	 
	class SubClass extends start {
		
		void subDisplay() {
			
			a = a+100;
			b = b+100;
			//int d=c+1; private only access inside of the class
			System.out.println("Protected Variable : "+b);
			System.out.println("Public Variable : "+a);
		}
	}

	class encap {
		
		public static void main(String args[]) {		
		
			SubClass subObj = new SubClass();
			
			subObj.privateDisplay();
			subObj.subDisplay();
		}
	
	}

